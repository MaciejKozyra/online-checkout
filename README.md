# Online Checkout
Simple web application with online checkout functionality.
## General info
 This web app is a checkout for online shop with it's functionality. The app is front-end only, therefore it can still be improved by creating and connecting with a database.
## Technologies
The app is created with:
    * Vue.js 2,
    * @vue/cli 4.5.13
    * Vuetify,
    * vuetify@2.6.7
## Requirements 
This application requires NodeJs (v16.8.0) and NPM (7.21.0) to be installed. In order to check installed version try running following commands:

```
$ node -v & npm -v
```
## Setup
First step is to clone the repository to desired location, using following command:
```
with SSH:
$ git clone git@gitlab.com:MaciejKozyra/online-checkout.git
or
with HTTPS:
$ git clone https://gitlab.com/MaciejKozyra/online-checkout.git 
```
To install dependencies please run: 
```
$ npm install
```
To compile the app make sure to be inside project's folder and  run following command:
```
$ npm run serve
```